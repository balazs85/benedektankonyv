/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tankonyv;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import pojo.Diak;
import pojo.Diakkonyv;
import pojo.Diakosztalyev;
import pojo.Konyv;
import pojo.Targy;

/**
 *
 * @author nbalazs
 */
public class Dao {

    //<editor-fold defaultstate="collapsed" desc="targy">
    public void saveOrUpdateTargy(Targy targy) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(targy);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
    }

    public List<Targy> getTargyak() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Targy> eredmeny = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Targy.class);
            cr.addOrder(Order.asc("nev"));
            eredmeny = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return eredmeny;
    }

    public Targy getTargy(Integer targyId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        Targy targy = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Targy.class);
            cr.add(Restrictions.eq("id", targyId));
            targy = (Targy) cr.list().get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return targy;
    }

    public void deleteTargy(Targy targy) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.delete(targy);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="konyv">
    public void saveOrUpdateKonyv(Konyv konyv) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(konyv);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
    }

    public List<Konyv> getKonyvek() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Konyv> eredmeny = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Konyv.class);
            cr.addOrder(Order.asc("cim"));
            eredmeny = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return eredmeny;
    }
    public List<Konyv> getKonyvek(Targy targy, String kodReszlet) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Konyv> eredmeny = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Konyv.class);
            if (targy != null){
                cr.add(Restrictions.eq("targy", targy));
            }
            if (kodReszlet != null){
                cr.add(Restrictions.like("kod", kodReszlet, MatchMode.ANYWHERE));
            }
            cr.addOrder(Order.asc("cim"));
            eredmeny = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return eredmeny;
    }

    public Konyv getKonyv(Integer konyvId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        Konyv konyv = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Konyv.class);
            cr.add(Restrictions.eq("id", konyvId));
            konyv = (Konyv) cr.list().get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return konyv;
    }

    public void deleteKonyv(Konyv konyv) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.delete(konyv);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="diak">
    public void saveOrUpdateDiak(Diak diak) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(diak);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
    }

    public List<Diak> getDiakok() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Diak> eredmeny = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Diak.class);
            cr.addOrder(Order.asc("nev"));
            eredmeny = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return eredmeny;
    }
    
    List<Diak> getDiakok(String kivalasztottEvDiaklistazashoz) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Diak> eredmeny = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Diak.class);
            cr.createAlias("diakosztalyevs", "doe");
            cr.add(Restrictions.eq("doe.tanev", kivalasztottEvDiaklistazashoz));
            cr.addOrder(Order.asc("nev"));
            eredmeny = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return eredmeny;
    }

    List<Diak> getDiakok(String kivalasztottEvDiaklistazashoz, String kivalasztottOsztalyDiaklistazashoz) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Diak> eredmeny = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Diak.class);
            cr.createAlias("diakosztalyevs", "doe");
            cr.add(Restrictions.eq("doe.tanev", kivalasztottEvDiaklistazashoz));
            cr.add(Restrictions.eq("doe.osztaly", kivalasztottOsztalyDiaklistazashoz));
            cr.addOrder(Order.asc("nev"));
            eredmeny = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return eredmeny;
    }


    public Diak getDiak(Integer diakId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        Diak diak = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Diak.class);
            cr.add(Restrictions.eq("id", diakId));
            diak = (Diak) cr.list().get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return diak;
    }

    public void deleteDiak(Diak diak) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.delete(diak);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="diakosztalyev">

//    public Diakosztalyev existsDiakosztalyev(Diakosztalyev diakosztalyev) {
//        for (Diakosztalyev d : diakosztalyev.getDiak().getDiakosztalyevs()) {
//            if (d.equals(diakosztalyev)) {
//                return d;
//            }
//        }
//        return null;
//    }

    public void saveOrUpdateDiakosztalyev(Diakosztalyev diakosztalyev) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(diakosztalyev);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
    }
    
    public void deleteDiakosztalyev(Diakosztalyev diakosztalyev) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            //session.delete(new Diakosztalyev(diak, osztalyev));
            session.delete(diakosztalyev);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
    }
    
    public List<Diakosztalyev> getDiakosztalyevek(Diak d) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Diakosztalyev> eredmeny = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Diakosztalyev.class);
            cr.add(Restrictions.eq("diakId", d.getId()));
            cr.addOrder(Order.asc("ev"));
            eredmeny = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return eredmeny;
    }
    
    
    List<Diakosztalyev> getDiakosztalyevek(String tanev) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Diakosztalyev> eredmeny = null;
        //CsopOsztaly
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Diakosztalyev.class);
            cr.add(Restrictions.eq("tanev", tanev));
            cr.setProjection(Projections.groupProperty("osztaly"));
            cr.addOrder(Order.asc("osztaly"));
            eredmeny = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return eredmeny;
    }

    public Diakosztalyev getDiakosztalyev(Integer diakOsztalyEvId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        Diakosztalyev diakOsztalyEv = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Diakosztalyev.class);
            cr.add(Restrictions.eq("id", diakOsztalyEvId));
            diakOsztalyEv = (Diakosztalyev) cr.list().get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return diakOsztalyEv;
    }
    
    public List<Diakosztalyev> getDiakosztalytanevek() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Diakosztalyev> eredmeny = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Diakosztalyev.class);
            cr.setProjection(Projections.groupProperty("tanev"));
            cr.addOrder(Order.asc("tanev"));
            eredmeny = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return eredmeny;
    }

    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="diakKonyv">
        public void saveOrUpdateDiakkonyv(Diakkonyv termek) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(termek);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
    }

    public List<Diakkonyv> getDiakkonyvek() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Diakkonyv> eredmeny = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Diakkonyv.class, "diakkonyv");
            cr.createAlias("diakkonyv.konyv", "konyv");
            cr.addOrder(Order.asc("konyv.cim"));
            eredmeny = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return eredmeny;
    }

    public List<Diakkonyv> getDiakkonyvek(Konyv konyv) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Diakkonyv> eredmeny = null;

        try {
            tx = session.beginTransaction();    //itt tartok
            Criteria cr = session.createCriteria(Diakkonyv.class, "diakkonyv");
            cr.add(Restrictions.eq("konyv", konyv));
//            cr.add(Restrictions.eq("diakosztalyev.diak", diak));
            cr.createAlias("diakkonyv.konyv", "konyv");
//            cr.createAlias("diakkonyv.diakosztalyev", "diakosztalyev");
            cr.addOrder(Order.asc("konyv.cim"));
            eredmeny = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return eredmeny;
    }

    
    public List<Diakkonyv> getDiakkonyvek(Diak diak) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Diakkonyv> eredmeny = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Diakkonyv.class, "diakkonyv");
            //cr.add(Restrictions.eq("diakosztalyev", diakosztalyev));
            cr.add(Restrictions.eq("diakosztalyev.diak", diak));
            cr.createAlias("diakkonyv.konyv", "konyv");
            cr.createAlias("diakkonyv.diakosztalyev", "diakosztalyev");
            cr.addOrder(Order.asc("konyv.cim"));
            eredmeny = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return eredmeny;
    }

    public List<Diakkonyv> getDiakkonyvek(Diakosztalyev diakosztalyev) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Diakkonyv> eredmeny = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Diakkonyv.class, "diakkonyv");
            cr.add(Restrictions.eq("diakosztalyev", diakosztalyev));
            cr.createAlias("diakkonyv.konyv", "konyv");
            cr.addOrder(Order.asc("konyv.cim"));
            eredmeny = cr.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return eredmeny;
    }

    public Diakkonyv getDiakkonyv(Integer diakkonyvId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        Diakkonyv termek = null;

        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Diakkonyv.class);
            cr.add(Restrictions.eq("id", diakkonyvId));
            termek = (Diakkonyv) cr.list().get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return termek;
    }
    
    public void deleteDiakkonyv(Diakkonyv termek) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.delete(termek);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
    }

    //</editor-fold>



    
}
