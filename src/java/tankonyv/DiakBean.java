/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tankonyv;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import org.hibernate.mapping.Collection;
import pojo.Diak;
import pojo.Diakkonyv;
import pojo.Diakosztalyev;
import pojo.Konyv;

/**
 *
 * @author nbalazs
 */
@ManagedBean(name = "diakBean")
@SessionScoped
public class DiakBean {

    @ManagedProperty(value = "#{konyvBean}")
    private KonyvBean konyvBean;
    
    private String kivalasztottEvDiaklistazashoz;
    private List<Diakosztalyev> evekDiaklistazashoz;
    private String kivalasztottOsztalyDiaklistazashoz;
    private List<Diakosztalyev> osztalyokDiaklistazashoz;

    private Diakkonyv kivalasztottDiakKonyv;
    private Diakosztalyev kivalasztottDiakOsztalyEv;
    private Dao dao;
    private Diak kivalasztottDiak;
    private List<Diak> diakok;
    private DataModel<Diakkonyv> diakkonyvek;
//    private List<Diakosztalyev> diakOsztalyEvek;
//    private List<Diakkonyv> diakKonyvek;

    public List<Diak> updateDiakok() {
        if (kivalasztottEvDiaklistazashoz == null) {
            System.out.println("Egyik");
            diakok = dao.getDiakok();
        } else if (kivalasztottOsztalyDiaklistazashoz == null) {
            System.out.println("Masik");
            diakok = dao.getDiakok(kivalasztottEvDiaklistazashoz);
        } else {
            System.out.println("Harmadik");
            diakok = dao.getDiakok(kivalasztottEvDiaklistazashoz, kivalasztottOsztalyDiaklistazashoz);
        }
        
        return getDiakok();
    }

    //property
    public List<Diakosztalyev> getDiakOsztalyEvekDiaklistazashoz() {
        return osztalyokDiaklistazashoz;
    }

    public String getKivalasztottOsztalyDiaklistazashoz() {
        return kivalasztottOsztalyDiaklistazashoz;
    }

    public void setKivalasztottOsztalyDiaklistazashoz(String kivalasztottOsztalyDiaklistazashoz) {
        this.kivalasztottOsztalyDiaklistazashoz = kivalasztottOsztalyDiaklistazashoz;
        kivalasztottDiak = null;
        updateDiakok();
    }

    

    public String getKivalasztottEvDiaklistazashoz() {
        return kivalasztottEvDiaklistazashoz;
    }

    public void setKivalasztottEvDiaklistazashoz(String kivalasztottEvDiakListazashoz) {
        this.kivalasztottEvDiaklistazashoz = kivalasztottEvDiakListazashoz;
        if (kivalasztottEvDiakListazashoz == null) {
            this.osztalyokDiaklistazashoz = null;
        } else {
            this.osztalyokDiaklistazashoz = dao.getDiakosztalyevek(kivalasztottEvDiakListazashoz);
        }
        kivalasztottDiak = null;
        updateDiakok();
    }

    public List<Diakosztalyev> getEvekDiaklistazashoz() {
        evekDiaklistazashoz = dao.getDiakosztalytanevek();
        return evekDiaklistazashoz;
    }

    public KonyvBean getKonyvBean() {
        return konyvBean;
    }

    public void setKonyvBean(KonyvBean konyvBean) {
        this.konyvBean = konyvBean;
    }

    public DataModel<Diakkonyv> getDiakkonyvek() {
        if (kivalasztottDiakOsztalyEv != null && kivalasztottDiakOsztalyEv.getTanev() != null) {
//            System.out.println(kivalasztottDiakOsztalyEv.getTanev());
            List<Diakkonyv> list = dao.getDiakkonyvek(kivalasztottDiakOsztalyEv);
            diakkonyvek = new ListDataModel<Diakkonyv>(list);
            return diakkonyvek;
        }
        if (kivalasztottDiak != null) {
//            System.out.println(kivalasztottDiak.getNev());
            List<Diakkonyv> list = dao.getDiakkonyvek(kivalasztottDiak);
            diakkonyvek = new ListDataModel<Diakkonyv>(list);
            return diakkonyvek;
        }
        return new ListDataModel<>();
    }
//    
//    public DataModel<Diakkonyv> getDiakkonyvsModel(){
//        
//    }

    public Integer getKivalasztottDiakKonyvId() {
        if (kivalasztottDiakKonyv == null) {
            return null;
        } else {
            return kivalasztottDiakKonyv.getId();
        }
    }

    public void setKivalasztottDiakKonyvId(Integer kivalasztottDiakKonyvId) {
        if (kivalasztottDiakKonyv != null) {
            this.kivalasztottDiakKonyv = dao.getDiakkonyv(kivalasztottDiakKonyvId);
        } else {
            this.kivalasztottDiakKonyv = null;
        }
    }

    public Diakkonyv getKivaloasztottDiakkonyv() {
        return kivalasztottDiakKonyv;
    }

    public void setKivaloasztottDiakkonyv(Diakkonyv kivaloasztottDiakkonyv) {
        this.kivalasztottDiakKonyv = kivaloasztottDiakkonyv;
    }

    public Integer getKivalasztottDiakOsztalyevId() {
        if (kivalasztottDiakOsztalyEv == null) {
            return null;
        } else {
            return kivalasztottDiakOsztalyEv.getId();
        }
    }

    public void setKivalasztottDiakOsztalyevId(Integer kivalasztottDiakOsztalyevId) {
        if (kivalasztottDiakOsztalyevId != null) {
            this.kivalasztottDiakOsztalyEv = dao.getDiakosztalyev(kivalasztottDiakOsztalyevId);
        } else {
            this.kivalasztottDiakOsztalyEv = null;
        }
    }

//    public List<Diakosztalyev> getDiakOsztalyEvek() {
//        return diakOsztalyEvek;
//    }
//
//    public void setDiakOsztalyEvek(List<Diakosztalyev> diakOsztalyEvek) {
//        this.diakOsztalyEvek = diakOsztalyEvek;
//    }
//    
    public Diakosztalyev getKivalasztottDiakOsztalyev() {
        return kivalasztottDiakOsztalyEv;
    }

    public void setKivalasztottDiakOsztalyev(Diakosztalyev kivalasztottDiakOsztalyev) {
        this.kivalasztottDiakOsztalyEv = kivalasztottDiakOsztalyev;
    }

    public Integer getKivalasztottDiakId() {
        if (kivalasztottDiak == null) {
            return null;
        } else {
            return kivalasztottDiak.getId();
        }
    }

    public void setKivalasztottDiakId(Integer kivalasztottDiakId) {
        kivalasztottDiakOsztalyEv = null;
        kivalasztottDiakKonyv = null;
        diakkonyvek = null;
        if (kivalasztottDiakId != null) {
            this.kivalasztottDiak = dao.getDiak(kivalasztottDiakId);
        } else {
            this.kivalasztottDiak = null;
        }
    }

    public Diak getKivalasztottDiak() {
        return kivalasztottDiak;
    }

    public void setKivalasztottDiak(Diak kivalasztottDiak) {
        this.kivalasztottDiak = kivalasztottDiak;
    }

    public List<Diak> getDiakok() {
        return diakok;
    }

//    public void setDiakok(List<Diak> diakok) {
//        this.diakok = diakok;
//    }
//    public List<Diakkonyv> getDiakKonyvek() {
//        return diakKonyvek;
//    }
//
//    public void setDiakKonyvek(List<Diakkonyv> diakKonyvek) {
//        this.diakKonyvek = diakKonyvek;
//    }
    //AJAX függvények
    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return null;
    }

    public String ujDiak() {
        kivalasztottDiak = new Diak();
        return null;
    }

    public String mentDiak() {
        dao.saveOrUpdateDiak(kivalasztottDiak);
        diakok = updateDiakok();
        return null;
    }

    public String torolDiak() {
        dao.deleteDiak(kivalasztottDiak);
        diakok = updateDiakok();
        kivalasztottDiak = null;
        return null;
    }

    public List<Diakosztalyev> getDiakosztalyevs() {
        if (kivalasztottDiak != null && kivalasztottDiak.getDiakosztalyevs() != null) {
            List<Diakosztalyev> diakosztalyevs = new ArrayList<>(kivalasztottDiak.getDiakosztalyevs());
            diakosztalyevs.sort(new Comparator<Diakosztalyev>() {
                @Override
                public int compare(Diakosztalyev o1, Diakosztalyev o2) {
                    return o1.getTanev().compareTo(o2.getTanev());
                }
            });
            return diakosztalyevs;
        }
        return null;
    }

    public String ujDiakOsztalyev() {
        kivalasztottDiakOsztalyEv = new Diakosztalyev();
        kivalasztottDiakOsztalyEv.setDiak(kivalasztottDiak);
        konyvBean.updateKonyvek();  //nem frissítik a könyvek listáját, még mindig megjelennek
        konyvBean.getKonyvek();
        return null;
    }

    public String mentDiakOsztalyev() {
        dao.saveOrUpdateDiakosztalyev(kivalasztottDiakOsztalyEv);
        kivalasztottDiak = dao.getDiak(kivalasztottDiak.getId());   //újra lekér a diákot, mert megváltoztak az évei
//        diakOsztalyEvek = dao.getDiakosztalyevek(kivalasztottDiak);
        return null;

//        System.out.println(kivalasztottOsztalyev.getOsztaly());
//        System.out.println(kivalasztottOsztalyev.getTanev());
//        dao.saveOrUpdateOsztalyev(kivalasztottOsztalyev);
//        Diakosztalyev diakosztalyev = new Diakosztalyev();
//        diakosztalyev.setDiak(kivalasztottDiak);
//        diakosztalyev.setOsztalyev(kivalasztottOsztalyev);
//        dao.saveOrUpdateDiakosztalyev(diakosztalyev);
//        //újra lekérjük a diákot -> megváltozhatott az osztályév
//        kivalasztottDiak = dao.getDiak(kivalasztottDiak.getId());
    }

    public String torolDiakOsztalyev() {
        dao.deleteDiakosztalyev(kivalasztottDiakOsztalyEv);
        kivalasztottDiak = dao.getDiak(kivalasztottDiak.getId());   //újrakér, mert megváltoztak az évei
        kivalasztottDiakOsztalyEv = null;
        return null;
    }

    public String selejtez() {
        Diakkonyv dk = diakkonyvek.getRowData();
        dao.deleteDiakkonyv(dk);
        getDiakkonyvek();
        //System.out.println(dk.getKonyv().getCim());
        return null;
    }

    public String kivesz() {
        Konyv k = konyvBean.getKonyvek().getRowData();
        Diakosztalyev d = kivalasztottDiakOsztalyEv;
        Diakkonyv dk = new Diakkonyv(d, k, new Date(), null);
        k.setDbKonyvtarban(k.getDbKonyvtarban() - 1);
        dao.saveOrUpdateKonyv(k);
        dao.saveOrUpdateDiakkonyv(dk);
        getDiakkonyvek();
        getKonyvBean().getKonyvek();
        return null;
    }

//    public void updateKivalasztottDiak(){
//        kivalasztottDiakOsztalyEv = dao.get
//        kivalasztottDiak = dao.getDiak(kivalasztottDiak.getId());
//    }
    public String visszahoz() {
        Diakkonyv dk = diakkonyvek.getRowData();
        Konyv k = dk.getKonyv();
        k.setDbKonyvtarban(k.getDbKonyvtarban() + 1);
        dao.saveOrUpdateKonyv(k);
        //System.out.println(k.getDbKonyvtarban());
        dao.deleteDiakkonyv(dk);
        getDiakkonyvek();
        getKonyvBean().updateKonyvek();     //könyvlista frissüljenek a darabszámok
        getKonyvBean().getKonyvek();
        //updateKivalasztottDiak();           //frissíti a diák éveit->töröl anabled false
        //getKivalasztottDiak();  
        //kivalasztottDiakOsztalyEv = getKivalasztottDiakOsztalyev();
        return null;
    }
    
    public void pdfKeszit(){
        PdfKeszito p = new PdfKeszito(getDiakosztalyevs());
//        PdfKeszito p = new PdfKeszito();
        p.elkeszit();
    }
    
    public String szurKodreszletAlapjan(){
        return null;
    }

    public DiakBean() {
        dao = new Dao();
        
        diakok = updateDiakok();
    }

}
