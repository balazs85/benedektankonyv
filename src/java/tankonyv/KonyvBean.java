/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tankonyv;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import pojo.Diak;
import pojo.Diakkonyv;
import pojo.Konyv;
import pojo.Targy;

/**
 *
 * @author nbalazs
 */
@ManagedBean(name = "konyvBean")
@SessionScoped
public class KonyvBean {

    //private Integer kivalasztottTargyId;
    private Targy kivalasztottTargy;
    private Konyv kivalasztottKonyv;
    private String kodReszlet;
    private Dao dao;
    private DataModel<Konyv> konyvek;
    private DataModel<Diakkonyv> diakkonyvek;
    //private DataModel<Konyv> konyvek;
    //private List<Konyv> konyvek;
    private List<Targy> targyak;

    public Integer getKivalasztottTargyId() {
        if (kivalasztottTargy == null) {
            return null;
        } else {
            return kivalasztottTargy.getId();
        }
    }

    public String getKodReszlet() {
        return kodReszlet;
    }

    public void setKodReszlet(String kodReszlet) {
        this.kodReszlet = kodReszlet;
        konyvek = new ListDataModel<Konyv>(dao.getKonyvek(kivalasztottTargy, kodReszlet));
    }
    
    public String csakGomb(){
        System.out.println("Hello");
        return null;
    }
    
    public void setKivalasztottTargyId(Integer kivalasztottTargyId) {
        if (kivalasztottTargyId != null) {
            this.kivalasztottTargy = dao.getTargy(kivalasztottTargyId);
        } else {
            this.kivalasztottTargy = null;
        }
        konyvek = new ListDataModel<Konyv>(dao.getKonyvek(kivalasztottTargy, kodReszlet));
        kivalasztottKonyv = null;
    }

    public Targy getKivalasztottTargy() {
        return kivalasztottTargy;
    }
    
    public Integer getKivalasztottKonyvTargyId() {
        if (kivalasztottKonyv == null || kivalasztottKonyv.getTargy() == null) {
            return null;
        } else {
            return kivalasztottKonyv.getTargy().getId();
        }
    }

    public void setKivalasztottKonyvTargyId(Integer kivalasztottTargyId) {
            if (kivalasztottTargyId != null && kivalasztottTargyId != 0) {
                kivalasztottKonyv.setTargy(dao.getTargy(kivalasztottTargyId));
                //this.kivalasztottKonyv.setTargy();
                //this.kivalasztottKonyv = dao.getKonyv(kivalasztottKonyvId);
            } else {
                kivalasztottKonyv.setTargy(null);
            }
    }

    public Integer getKivalasztottKonyvId() {
        if (kivalasztottKonyv == null) {
            return null;
        } else {
            return kivalasztottKonyv.getId();
        }
    }

    public void setKivalasztottKonyvId(Integer kivalasztottKonyvId) {
        if (kivalasztottKonyvId != null) {
            this.kivalasztottKonyv = dao.getKonyv(kivalasztottKonyvId);
        } else {
            this.kivalasztottKonyv = null;
        }
    }

    public Konyv getKivalasztottKonyv() {
        return kivalasztottKonyv;
    }

    public void setKivalasztottKonyv(Konyv kivalasztottKonyv) {
        this.kivalasztottKonyv = kivalasztottKonyv;
    }

    public DataModel<Konyv> getKonyvek() {
        return konyvek;
    }

    public List<Targy> getTargyak() {
        return targyak;
    }
    
    public void updateKonyvek(){
        konyvek = new ListDataModel<Konyv>(dao.getKonyvek());
    }

    public DataModel<Diakkonyv> getDiakkonyvek() {
        if (kivalasztottKonyv != null){
            List<Diakkonyv> list = dao.getDiakkonyvek(kivalasztottKonyv);
            diakkonyvek = new ListDataModel<Diakkonyv>(list);
            return diakkonyvek;
        } 
        return null;
    }

    
    

    //AJAX függvények
    public String ujKonyv() {
        kivalasztottKonyv = new Konyv();
        return null;
    }

    public String mentKonyv() {

        dao.saveOrUpdateKonyv(kivalasztottKonyv);
        konyvek = new ListDataModel<Konyv>(dao.getKonyvek());
        return null;
    }

    public String torolKonyv() {
        dao.deleteKonyv(kivalasztottKonyv);
        konyvek = new ListDataModel<Konyv>(dao.getKonyvek());
        kivalasztottKonyv = null;
        return null;
    }

    public String ujTargy() {
        kivalasztottTargy = new Targy();
        return null;
    }

    public String mentTargy() {
        dao.saveOrUpdateTargy(kivalasztottTargy);
        targyak = dao.getTargyak();
        return null;
    }

    public String torolTargy() {
        //System.out.println(kivalasztottKonyv.getTargy().getKonyvs().size());
        if (kivalasztottTargy.getKonyvs().size() <= 1) {
            dao.deleteTargy(kivalasztottTargy);
            targyak = dao.getTargyak();
            kivalasztottTargy = null;
        }
        return null;
    }

    /**
     * Creates a new instance of KonyvBean
     */
    public KonyvBean() {
        dao = new Dao();
        konyvek = new ListDataModel<Konyv>(dao.getKonyvek());
        targyak = dao.getTargyak();
    }

}
