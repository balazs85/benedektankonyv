package tankonyv;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import static com.itextpdf.text.pdf.PdfDictionary.FONT;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import pojo.Diakkonyv;
import pojo.Diakosztalyev;

public class PdfKeszito {

    //private static final BaseFont bf = BaseFont.createFont(Font.FontFamily.TIMES_ROMAN, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
    private static Font cimFont = new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD);
    private static Font nevFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static Font alCimFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
    private static Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

    private static Font tablazatFejFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private static Font tablazatFont = new Font(Font.FontFamily.TIMES_ROMAN, 12);

    private List<Diakosztalyev> diakosztalyevs;

    public PdfKeszito(List<Diakosztalyev> diakosztalyevs) {
        this.diakosztalyevs = diakosztalyevs;
        try {
            BaseFont timesBaseFont = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
            normalFont = new Font(timesBaseFont, 12, Font.NORMAL);
            nevFont = new Font(timesBaseFont, 18, Font.BOLD);
        } catch (DocumentException ex) {
        } catch (IOException ex) {
        }

    }
/*    
    public void elkeszit() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.setContentType("application/pdf");
        response.setHeader("Content-disposition", "inline=filename=file.pdf");
        try {

            // Get the text that will be added to the PDF
            String text = "test";
            // step 1
            Document document = new Document();
            // step 2
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PdfWriter.getInstance(document, baos);
            // step 3
            document.open();
            // step 4
            document.add(new Paragraph(text));
            // step 5
            document.close();

            // setting some response headers
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control",
                    "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            // setting the content type
            response.setContentType("application/pdf");
            // the contentlength
            response.setContentLength(baos.size());
            // write ByteArrayOutputStream to the ServletOutputStream
            ServletOutputStream os = response.getOutputStream();
            baos.writeTo(os);
            os.flush();
            os.close();

        } catch (DocumentException e) {
        } catch (IOException e) {
        } catch (Exception ex) {
        }
        context.responseComplete();
    }
*/

    public void elkeszit() {
        String fileName = diakosztalyevs.get(0).getDiak().getNev()+".pdf";
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.setContentType("application/pdf");
        //response.setHeader("Content-disposition", "inline=filename=file.pdf");
        try {
            response.setHeader("Content-Disposition", "attachment; filename=\"" 
                + URLEncoder.encode(fileName.replace(" ", ""), java.nio.charset.StandardCharsets.UTF_8.toString()) + "\"");
            Document document = new Document();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PdfWriter.getInstance(document, baos);
            //PdfWriter.getInstance(document, new FileOutputStream(fNev));
            document.open();
            
            //String text = "test";
            //document.add(new Paragraph(text));
            
            metaAdatok(document);
            tartalom(document);
            document.close();

            // setting some response headers
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control",
                    "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            // setting the content type
            response.setContentType("application/pdf");
            // the contentlength
            response.setContentLength(baos.size());
            // write ByteArrayOutputStream to the ServletOutputStream
            OutputStream os = response.getOutputStream();
            baos.writeTo(os);
            os.flush();
            os.close();
        } catch (FileNotFoundException fileNotFoundException) {
            System.out.println(fileNotFoundException.getStackTrace());
        } catch (DocumentException documentException) {
            System.out.println(documentException.getStackTrace());
        } catch (IOException ex) {
            System.out.println(ex.getStackTrace());

        }
        context.responseComplete();
    }

    private void tartalom(Document document) {
        try {
            Paragraph iskola = new Paragraph("Szent Benedek Iskolaközpont\n", normalFont);
            iskola.add("Mezőkövesd u. 10\n");
            iskola.add("Buddapest\n");
            iskola.add("1116");
            iskola.setIndentationLeft(350);
            document.add(iskola);
            Paragraph cim = new Paragraph("Tanulói tankönyvlista", cimFont);
            cim.setSpacingBefore(32);
            cim.setAlignment(Element.ALIGN_CENTER);
            cim.setSpacingAfter(48);
            document.add(cim);
            Paragraph nev = new Paragraph(diakosztalyevs.get(0).getDiak().getNev(), nevFont);
            document.add(nev);
            Paragraph uzenet = new Paragraph("Tisztelt Szülő!\nJelenleg az alábbi "
                    + "könyvtári állományú könyvek vannak gyermekénél. Kérem az iskola "
                    + "honlapján tájékozódjon, hogy ezekből a könyvekből tanáv "
                    + "végén mit kell leadni!", normalFont);
            document.add(uzenet);
            for (Diakosztalyev diakosztalyev : diakosztalyevs) {
                Paragraph ev = new Paragraph(diakosztalyev.getTanev() + " - "
                        + diakosztalyev.getOsztaly(), alCimFont);
                ev.setSpacingBefore(12);
                document.add(ev);
                List<Diakkonyv> konyvek = new ArrayList<>(diakosztalyev.getDiakkonyvs());
                konyvek.sort(new Comparator<Diakkonyv>() {

                    @Override
                    public int compare(Diakkonyv o1, Diakkonyv o2) {
                        return o1.getKonyv().getCim().compareTo(o2.getKonyv().getCim());
                    }
                });
                for (Diakkonyv konyv : konyvek) {
                    Paragraph konyvBekezdes = new Paragraph(konyv.getKonyv().getCim()
                            + " (" + konyv.getKonyv().getAr() + " Ft)", normalFont);
                    konyvBekezdes.setIndentationLeft(15);
                    document.add(konyvBekezdes);
                }
            }
            Date d = new Date();
            DateFormat df = new SimpleDateFormat("yyyy.mm.dd.");
            Paragraph lab = new Paragraph("Budapest, " + df.format(d));
            lab.setSpacingBefore(48);
            document.add(lab);

        } catch (DocumentException documentException) {
        }
    }

    private void metaAdatok(Document document) {
        document.addTitle("Az első PDF dokumentumom");
        document.addSubject("Az iText használata");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("Nagy Balázs Tamás");
        document.addCreator("Nagy Balázs Tamás");
    }

}
