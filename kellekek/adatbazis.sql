CREATE TABLE TTargy (
	TargyAzon INTEGER PRIMARY KEY,
	TargyNev Text
);

CREATE TABLE targy (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nev VARCHAR(100)
);

CREATE TABLE [TKonyv] (
[KonyvAzon] INTEGER  PRIMARY KEY NULL,
[KonyvCim] TEXT  NULL,
[Kod] TEXT  NULL,
[Tantargy] NUMERIC  NULL,
[Ar] NUMERIC  NULL,
[DB2] NUMERIC DEFAULT '0' NULL,
[DBKonyvtarban] NUMERIC DEFAULT '0' NULL
);

CREATE TABLE konyv (
    id INT AUTO_INCREMENT PRIMARY KEY,
    cim VARCHAR(200),
    kod VARCHAR(20),
    targyId INT,
    ar INT DEFAULT 0,
    db2 INT,
    dbKonyvtarban INT,
    FOREIGN KEY (targyId) REFERENCES targy(id)
);

CREATE TABLE [TDiak] (
[DiakAzon] INTEGER  PRIMARY KEY NULL,
[DiakNev] TEXT  NULL,
[OM] NVARCHAR(11)  NULL
);

CREATE TABLE diak (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nev VARCHAR(100),
    om VARCHAR(11)
);

CREATE TABLE TOsztalyEv (
	OsztalyEvAzon INTEGER PRIMARY KEY,
	Osztaly TEXT,
	Tanev TEXT
);

CREATE TABLE osztalyev (
    id INT AUTO_INCREMENT PRIMARY KEY,
    osztaly VARCHAR(10),
    tanev VARCHAR(10)
);

CREATE TABLE TDiakOsztalyEv (
	DiakAzon NUMERIC,
	OsztalyEvAzon NUMERIC,
	FOREIGN KEY(DiakAzon) REFERENCES TDiak(DiakAzon),
	FOREIGN KEY(OsztalyEvAzon) REFERENCES TOsztalyEv(OsztalyEvAzon),
	PRIMARY KEY (DiakAzon, OsztalyEvAzon)
);

CREATE TABLE diakosztalyev (
    id INT AUTO_INCREMENT PRIMARY KEY,
    diakId INT,
    osztalyEvId INT,
    FOREIGN KEY (diakId) REFERENCES diak(id),
    FOREIGN KEY (osztalyEvId) REFERENCES osztalyev(id)
);

CREATE TABLE [TDiakKonyv] (
[DiakAzon] NUMERIC  NULL,
[KonyvAzon] NUMERIC  NULL,
[OsztalyEvAzon] INTEGER  NULL,
[Ev] TEXT  NULL,
[Osztaly] TEXT  NULL,
[KintVan_e] INTEGER  NULL,
PRIMARY KEY ([DiakAzon],[KonyvAzon],[OsztalyEvAzon])
);

CREATE TABLE diakkonyv (
    id INT AUTO_INCREMENT PRIMARY KEY,
    diakId INT,
    konyvId INT,
    osztalyEvId INT,
    ev INT,
    osztaly INT,
    s DATE,
    visszavetelDatuma DATE,
    SelejtezesDatuma DATE,
    FOREIGN KEY (diakId) REFERENCES diak(id),
    FOREIGN KEY (konyvId) REFERENCES konyv(id),
    FOREIGN KEY (osztalyEvId) REFERENCES osztalyev(id)
);
