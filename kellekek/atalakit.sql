SELECT doe.id
FROM diakosztalyev doe, diakkonyv dk, osztalyev oe, diak d
WHERE dk.osztalyEvId = oe.id
AND oe.id = doe.osztalyEvId
AND dk.diakId = d.id
AND d.id = doe.diakId

UPDATE diakkonyv
SET diakkonyv.diakOsztalyEvId = diakosztalyev.id
WHERE diakkonyv.osztalyEvId = osztalyev.id
AND osztalyev.id = diakosztalyev.osztalyEvId
AND diakkonyv.diakId = diak.id
AND diak.id = diakosztalyev.diakId

SELECT diakosztalyev.id
FROM diakosztalyev, diakkonyv, osztalyev, diak
WHERE diakkonyv.osztalyEvId = osztalyev.id
AND osztalyev.id = diakosztalyev.osztalyEvId
AND diakkonyv.diakId = diak.id
AND diak.id = diakosztalyev.diakId

INSERT INTO dk (dk.diakOsztalyEvId)
SELECT doe.id
FROM diakosztalyev doe, diakkonyv dk, osztalyev oe, diak d
WHERE dk.osztalyEvId = oe.id
AND oe.id = doe.osztalyEvId
AND dk.diakId = d.id
AND d.id = doe.diakId

INSERT INTO diakkonyv (diakOsztalyEvId)
SELECT doe.id
FROM diakosztalyev doe, diakkonyv dk, osztalyev oe, diak d
WHERE dk.osztalyEvId = oe.id
AND oe.id = doe.osztalyEvId
AND dk.diakId = d.id
AND d.id = doe.diakId

SELECT doe.id
FROM diakosztalyev doe
INNER JOIN diakkonyv dk ON doe.osztalyEvId = dk.osztalyEvId
INNER JOIN diak d ON doe.diakId = d.id

UPDATE diakosztalyev doe
INNER JOIN diakkonyv dk ON doe.osztalyEvId = dk.osztalyEvId
INNER JOIN diak d ON doe.diakId = d.id
SET dk.diakOsztalyEvId = doe.id

ALTER TABLE diakkonyv
ADD FOREIGN KEY diakOsztalyEvId
REFERENCES diakosztalyev(id)

INSERT INTO diakosztalyev (diakId, osztalyEvId)
SELECT diakId, osztalyEvId
FROM diakkonyv 
where diakkonyv.diakOsztalyEvId is NULL

UPDATE diakosztalyev doe
INNER JOIN osztalyev oe ON doe.osztalyEvId = oe.id
SET doe.osztaly = oe.osztaly, 
doe.tanev = oe.tanev
